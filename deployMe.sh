#!/bin/bash -v

function buildAMIBase {
    #yum update -y

    ##if env variables need to be set set them here by uncommenting out the export lines
    # terraform will ensure they are there when deployed 
    #cd ~
    # sudo echo "export var=\"value\"" >> ~/.bashrc
    # sudo echo "export var2=\"value\"" >> ~/.bashrc
    # sudo echo "export var3=\"value\"" >> ~/.bashrc

    # serverURL is needed in httpd.conf
    sudo echo "export serverURL=\"SERVER_URL\"" >> ~/.bashrc
    source ~/.bashrc


    #install needed packages
    sudo yum install httpd -y
    sudo yum install php php-pgsql -y
    sudo yum install wget -y

    # install Shibbolth service provider and dependencies
    ./install_ShibbolthSpDependencies.sh

    # configure SP:

    # edit /etc/sysconfig/httpd
    sudo sed -i "s@#HTTPD=/usr/sbin/httpd.worker@HTTPD=/usr/sbin/httpd.worker@" /etc/sysconfig/httpd
    sudo su -c "echo \"export LD_LIBRARY_PATH=/opt/shibboleth-sp/lib\" >>  /etc/sysconfig/httpd"

    # edit /etc/httpd/conf/httpd.conf
    sudo sed -i "s/#ServerName www.example.com:80/ServerName $serverURL/" /etc/httpd/conf/httpd.conf
    sudo sed -i "s/UseCanonicalName Off/UseCanonicalName On/" /etc/httpd/conf/httpd.conf

    # copy shibboleth SP Apache configuration to apache conf.d directory
    apacheConfigDir=configurationFiles/apache/version2.2
    sudo cp $apacheConfigDir/apache22.conf /etc/httpd/conf.d/apache22.conf

    # copy shibboleth SP configuration files to shibboleth SP configuration directory
    shibSPConfigDir=configurationFiles/shibbolethSP/version2.5.5
    sudo cp $shibSPConfigDir/attribute-map.xml /opt/shibboleth-sp/etc/shibboleth/attribute-map.xml
    sudo cp $shibSPConfigDir/shibboleth2.xml /opt/shibboleth-sp/etc/shibboleth/shibboleth2.xml
    sudo cp $shibSPConfigDir/CirrusIdentitySocialProviders-metadata.xml /opt/shibboleth-sp/etc/shibboleth/CirrusIdentitySocialProviders-metadata.xml
    sudo cp $shibSPConfigDir/FederationMetadataCirrus.xml /opt/shibboleth-sp/etc/shibboleth/FederationMetadataCirrus.xml
    sudo cp $shibSPConfigDir/azure.xml /opt/shibboleth-sp/etc/shibboleth/azure.xml
    # need to copy sp-cert.pem to /opt/shibboleth-sp/etc/shibboleth/
    # need to copy sp-key.pem to /opt/shibboleth-sp/etc/shibboleth/
}

function installWebsite {
    # download newest build to tmp
    cd /tmp
    wget https://s3.amazonaws.com/dmc-frontend-distribution/DMCFrontendDist.zip 
    unzip DMCFrontendDist.zip  #code is now in /tmp/dist

    # move code to clean webroot and change owner to apache
    sudo rm -rf /var/www/html/*
    sudo mv /tmp/dist/* /var/www/html/.
    cd /var/www/html
    sudo chown -R apache:apache *
}

function httpToHttpsRewrite {
    sudo su -c "echo \"RewriteEngine on\" >>  /etc/httpd/conf/httpd.conf"
    sudo su -c "echo \"RewriteCond %{HTTP:X-Forwarded-Proto} ^http$\" >>  /etc/httpd/conf/httpd.conf"
    sudo su -c "echo \"RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]\" >>  /etc/httpd/conf/httpd.conf"
}

buildAMIBase

installWebsite

httpToHttpsRewrite

# start apache then shibboleth
service httpd start
sudo /opt/shibboleth-sp/sbin/shibd
