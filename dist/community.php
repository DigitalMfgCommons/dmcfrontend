<!doctype html> <html class=no-js> <head> <meta charset=utf-8> <meta name=description> <meta name=viewport content="width=device-width, initial-scale=1"> <title>Community</title> <link rel=apple-touch-icon href=apple-touch-icon.png>  <link rel=stylesheet href=styles/vendor.css> <link rel=stylesheet href=styles/main.css> <script src=scripts/vendor/modernizr.js></script> </head> <body ng-app=dmc.community> <!--[if lt IE 10]>
 <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 <![endif]-->  <div dmc-top-header active-page="'community'"></div> <ui-view></ui-view> <dmc-footer></dmc-footer> <script src=scripts/vendor.js></script> <script src=scripts/community/index.js></script> <script type=text/javascript>
        <?php
          echo('window.givenName = "'.$_SERVER['givenName'].'";');
        ?>
        window.apiUrl = '';
    </script> </body> </html> 