<!doctype html> <html class=no-js> <head> <meta charset=utf-8> <meta name=description> <meta name=viewport content="width=device-width, initial-scale=1"> <title>Digital Manufacturing Commons</title> <link rel=apple-touch-icon href=apple-touch-icon.png>  <link rel=stylesheet href=styles/vendor.css> <link rel=stylesheet href=styles/main.css> <script src=scripts/vendor/modernizr.js></script> </head> <body ng-app=dmc.home ng-controller=HomeCtr> <!--[if lt IE 10]>
 <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 <![endif]-->  <div dmc-top-header active-page="'index'"></div> <div class="container index-container" layout=row layout-align="center center" layout-padding> <div class=content-column layout=column ng-repeat="p in pages"> <a href={{p.href}} class=home-feature> <img src=images/{{p.img}} class=md-card-image alt={{p.name}}> <h2>{{p.name}}</h2> <p>{{p.text}}</p> <md-button class="md-raised md-primary">View</md-button> </a> </div> </div>  <dmc-footer type="'home'"></dmc-footer> <script src=scripts/vendor.js></script> <script src=scripts/home/index.js></script> <script type=text/javascript>
        <?php
          echo('window.givenName = "'.$_SERVER['givenName'].'";');
        ?>
        window.apiUrl = '';
    </script> </body> </html> 