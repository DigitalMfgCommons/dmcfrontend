#!/bin/bash  -v

function spacer {
    echo "##########################################"
    echo $@
    echo "##########################################"
}

function installPackage {
    cwd=`pwd`
    fullPath=$1
    fileName=$(basename $fullPath)
    if [ ! -e $fileName ]; then
        wget $fullPath
        tar --gunzip -xvf $fileName
    else
        spacer "File " $fileName " exists. Skipping download and untar."
    fi

    fileName=${fileName%.*.*}

    if [ -d $fileName ]; then
        cd $fileName
        shift # remove package URL from function argumant list                                                                                                                     
        configure_command=$@
        spacer $configure_command
        $configure_command >> /tmp/${fileName}_configure.log 2>&1
        spacer "make"
        make >> /tmp/${fileName}_make.log 2>&1
        make check >> /tmp/${fileName}_check.log 2>&1
        spacer "sudo make install"
        sudo make install >> /tmp/${fileName}_install.log 2>&1
    else
        spacer "No directory " $fileName " to build"
    fi

    cd $cwd #return to starting directory.                                                                                                                                         
}

sudo yum install boost-devel gcc gcc-c++ libcurl libcurl-devel openssl-devel zlib -y

# information on packages and configure commands can be found at https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPLinuxSourceBuild

log4shib_configure="./configure --disable-static --disable-doxygen --prefix=/opt/shibboleth-sp"
installPackage http://shibboleth.net/downloads/log4shib/1.0.9/log4shib-1.0.9.tar.gz $log4shib_configure

Xerces_C_configure="./configure --prefix=/opt/shibboleth-sp --disable-netaccessor-libcurl"
installPackage archive.apache.org/dist/xerces/c/3/sources/xerces-c-3.1.3.tar.gz $Xerces_C_configure

XML_Security_C_configure="./configure --without-xalan --disable-static --prefix=/opt/shibboleth-sp CPPFLAGS=-I/opt/shibboleth-sp/include LDFLAGS=-L/opt/shibboleth-sp/lib"
installPackage http://archive.apache.org/dist/santuario/c-library/xml-security-c-1.7.3.tar.gz $XML_Security_C_configure


XMLTooling_C_configure="./configure --with-log4shib=/opt/shibboleth-sp --prefix=/opt/shibboleth-sp -C CPPFLAGS=-I/opt/shibboleth-sp/include LDFLAGS=-L/opt/shibboleth-sp/lib"
installPackage http://shibboleth.net/downloads/c++-opensaml/2.5.5/xmltooling-1.5.5.tar.gz $XMLTooling_C_configure

OpenSAML_C_configure="./configure --with-log4shib=/opt/shibboleth-sp --prefix=/opt/shibboleth-sp -C"
installPackage http://shibboleth.net/downloads/c++-opensaml/2.5.5/opensaml-2.5.5.tar.gz $OpenSAML_C_configure



## additional dependancies are httpd-devel and soft links to both apr-config and apu-config
sudo yum install httpd-devel -y
sudo ln /usr/bin/apr-1-config /usr/bin/apr-config
sudo ln /usr/bin/apu-1-config /usr/bin/apu-config
# Shibboleth_configure command had been altered for AWS.
Shibboleth_configure="./configure --with-log4shib=/opt/shibboleth-sp --with-xmltooling=/usr/local --enable-apache-22 --with-apxs2=/usr/sbin/apxs --prefix=/opt/shibboleth-sp CPPFLAGS=-I/usr/include/apr-1 LDFLAGS=-L/usr/lib64/apr-1"
installPackage http://shibboleth.net/downloads/service-provider/2.5.5/shibboleth-sp-2.5.5.tar.gz $Shibboleth_configure
