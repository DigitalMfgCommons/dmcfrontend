#!/bin/bash 

cd /tmp

function updateS3 {
wget https://s3.amazonaws.com/dmc-frontend-distribution/DMCFrontendDist.zip 
unzip DMCFrontendDist.zip  #code is now in /tmp/dist

# move code to clean webroot and change owner to apache
sudo rm -rf /var/www/html/*
sudo mv /tmp/dist/* /var/www/html/.

}



# how to update from latest build S3
updateS3



cd /var/www/html
sudo chown -R apache:apache *

sudo service httpd start